function ADUserModel(json) {
    // this.dn = "";
    // this.userPrincipalName = "";
    this.username = "";
    this.email = "";
    this.fullname = "";
    // if(json.dn){
    //     this.dn = json.dn;
    // }
    // if(json.userPrincipalName){
    //     this.userPrincipalName = json.userPrincipalName;
    // }
    if(json.sAMAccountName){
        this.username = json.sAMAccountName;
    }
    if(json.mail){
        this.email = json.mail;
    }
    if(json.displayName){
        this.fullname = json.displayName;
    }
}
module.exports = ADUserModel