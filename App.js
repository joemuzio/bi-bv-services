const express = require('express');
const Cors = require('cors');
const BodyParser = require('body-parser');
const app = express();
const TaskController = require('./BVTaskController');

app.use(Cors());
app.use(BodyParser.json());
app.use(BodyParser.urlencoded({extended: true}));

app.post('/OAuth/Login', function (req, res) {
    TaskController.loginTokenTask(req.body.username, req.body.password, function (error, response) {
        if(error){ res.json(error); }
        else{ res.json(response); }
    })
});
app.get('/getAllUsersInGroup', function (req, res) {
    TaskController.getAllUsersInGroup(req.headers.token, req.headers.group, function (error, response) {
        if(error){ res.json(error); }
        else{ res.json(response); }
    })
});
app.listen(9000);
console.log("live on port 9000");