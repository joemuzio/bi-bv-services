// const querystring = require('querystring');

module.exports = {
    adConfig: {
        url: 'ldap://catmktg.com:389',
        baseDN: 'ou=cmc,dc=catmktg,dc=com',
        username: 'ip_admin',
        password: 'YouOweMe$50'
    },
    adDomain: {
        domain: "catmktg.com",
        extranetDomain: "extranet.catmktg.com"
    },
    adBIGroup: "DBA MicroStrategy",
    dbConfig: {
        host: "localhost",
        user: "ms_admin",
        password: "C@talina1",
        database: "BIChangeControl"
    },
    jwtConfig: {
        secret: "secret_for_change_control!!!"
    }
};