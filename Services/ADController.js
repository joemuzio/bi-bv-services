var ActiveDirectory = require('activedirectory');
const Config = require('../Config');

var ad = new ActiveDirectory(Config.adConfig);


module.exports = {
    authenticate: function (username, password, callback) {
        authenticate(username, password, Config.adDomain.domain, function (error) {
            if(error){
                // Check extranet if first authentication fails
                authenticate(username, password, Config.adDomain.extranetDomain, function (error) {
                    if(error){ callback('ERROR: ' + JSON.stringify(error)); }
                    else{ callback(null, 'Authenticated'); }
                })
            }
            else{
                callback(null, 'Authenticated');
            }
        })
    },
    getAllUsersInGroup: function (group, callback) {
        ad.getUsersForGroup(group, function (error, response) {
            if(error){
                callback(error); }
            else{
                callback(null, response); }
        });
    },
    belongsToGroup: function (username, group, callback) {
        ad.isUserMemberOf(username, group, function (error, response) {
            if(error){ callback(error); }
            else{
                callback(null, response);
            }
        })
    }
};
function authenticate(username, password, domain, callback){
    ad.authenticate(username +'@' + domain, password, function(err, auth) {
        if (err) {
            callback('ERROR: ' + JSON.stringify(err));
        }
        else{
            console.log(auth);
            callback(null, 'Authenticated')
        }
    })
}