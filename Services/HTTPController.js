const request = require('request');

module.exports = {
    sendPost: function (urlOptions, callback) {
        request.post(urlOptions, function (error, response, body) {
            if(error){callback(error);}
            else{ callback(null, body);}
        })
    },
    sendGet: function (urlOptions, callback) {
        request.get(urlOptions, function (error, response, body) {
            if(error){callback(error);}
            else{ callback(null, body);}
        })
    }
};