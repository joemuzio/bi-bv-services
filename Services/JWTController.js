const JWT = require('jsonwebtoken');
const Config = require('../Config');

module.exports = {
    getToken: function (username) {
        return JWT.sign({username: username}, Config.jwtConfig.secret);
    },
    tokenIsValid: function (token) {
        var isValid = false;
        JWT.verify(token, Config.jwtConfig.secret, function (error, decoded) {
            if(decoded){
                isValid = true;
            }
        });
        return isValid;
    },
    usernameFromToken: function (token) {
        var username = "";
        JWT.verify(token, Config.jwtConfig.secret, function (error, decoded) {
            if(decoded){
                username = decoded.username;
            }
        });
        return username;
    }
};