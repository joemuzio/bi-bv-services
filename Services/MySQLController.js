const MYSQL = require('MYSQL');
const Config = require('../Config');
var pool = MYSQL.createPool(Config.dbConfig);

module.exports = {
    insertIntoTable: function (insertStatment, data, callback) {
        pool.getConnection(function (err, connection) {
            if(err){ callback(err); }
            else{
                connection.query(insertStatment, data, function (error) {
                    connection.release();
                    if(error){ callback(error); }
                    else{ callback(null, "Update Successful"); }
                })
            }
        })

    },
    selectFromTable: function (query, callback) {
        pool.getConnection(function (error, connection) {
            if(error){ callback(error); }
            else{
                connection.query(query, function (err, response) {
                    connection.release();
                    if(err){ callback(err); }
                    else{ callback(null, response); }
                })
            }
        })
    }
};
