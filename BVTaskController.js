const async = require('async');
const ADController = require('./Services/ADController');
const JWTController = require('./Services/JWTController');
const ADUserModel = require('./Models/ADUserModel');
const Config = require('./Config');
const MySQLController = require('./Services/MySQLController');

module.exports = {
    loginTokenTask: function (username, password, callback) {
        async.series(
            [
                async.apply(ADLogin, username, password),
                async.apply(belongsToBIGroup, username)
            ],
            function (error) {
                if(error){ callback(error); }
                else{ callback(null, {token: JWTController.getToken(username)}); }
            }
        )
    },
    getAllUsersInGroup:function (token, group, callback) {
        if(JWTController.tokenIsValid(token)){
            ADController.getAllUsersInGroup(group, function (error, response) {
                if(error){ callback(error); }
                else{
                    var usersArray = [];
                    for(var i in response){
                        const userModel = new ADUserModel(response[i]);
                        usersArray.push(userModel);
                    }
                    callback(null, usersArray); }
            })
        }
        else{
            callback(null, {error: "Token not valid"});
        }
    }
};

//Login Token Functions//
function ADLogin(username, password, callback) {
    ADController.authenticate(username, password, function (error) {
        if(error){ callback(error); }
        else{ callback(null, 'Authenticated'); }
    });
}
function belongsToBIGroup(username, callback) {
    ADController.belongsToGroup(username, Config.adBIGroup, function (error, response) {
        if(error){ callback(error); }
        else{
            if(response == true){
                callback(null, "belongs to group " + Config.adBIGroup);
            }
            else{ callback({error: "You must be a member of " + Config.adBIGroup + " to access this site."}); }
        }
    })
}
